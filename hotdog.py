import time
import re
import datetime
import json
from slackclient import SlackClient

TOKEN = "xoxb-2969658925-432458656577-xoYPkeKbKRssbET16gNKhqUY"

# instantiate Slack client
slack_client = SlackClient(TOKEN)
# starterbot's user ID in Slack: value is assigned after the bot starts up
starterbot_id = None

# constants
RTM_READ_DELAY = 1 # 1 second delay between reading from RTM

# keywords
ADDED = "added"
BOOK = "book"
UNBOOK = "unbook"
HELP = "help"

NOTIFY = "notify"

HOTDOG_MAX = 10
HOTDOGS_BOOKED = 0
HOTDOGS_INSIDE = 0

BOOKED_DICTIONARY = {}
EXTRA_DICT = {}

MENTION_REGEX = "^<@(|[WU].+?)>(.*)"

CHANNEL_GLOB = "GCQQ2PR44"


def parse_bot_commands(slack_events):
    """
        Parses a list of events coming from the Slack RTM API to find bot commands.
        If a bot command is found, this function returns a tuple of command and channel.
        If its not found, then this function returns None, None.
    """
    for event in slack_events:
        if event["type"] == "message" and not "subtype" in event:
            user_id, message = parse_direct_mention(event["text"])
            if user_id == starterbot_id:
                return message, event["channel"], event["user"]
    return None, None, None


def parse_direct_mention(message_text):
    """
        Finds a direct mention (a mention that is at the beginning) in message text
        and returns the user ID which was mentioned. If there is no direct mention, returns None
    """
    matches = re.search(MENTION_REGEX, message_text)
    # the first group contains the username, the second group contains the remaining message
    return (matches.group(1), matches.group(2).strip()) if matches else (None, None)


def handle_command(command, channel, user):
    """
        Executes bot command if the command is known
    """
    response = "Unknown action. Please tag me and then follow with 'added', 'book' or 'unbook' depending on the " +\
               "action you want to take and number of hotdogs. Example @Hotdog book 3"

    hotdog_req = get_number(command)
    user_tag = '<@'+user+'>'

    if command.startswith(NOTIFY):
        response = notify(user)

    if command.startswith(HELP):
        response = "Hello. If you have just added hotdogs to the machine then please tag me and say you have." +\
            "\n Example: @Hotdog added 10" +\
            "\n If you want to book some hotdogs then tag me and say so." + \
            "\n Example: @Hotdog book 3" + \
            "\n If you have changed your mind then please unbook and let your fellow Wanderans enjoy the prepared" \
            "hot dogs." + "\n Example: @Hotdog unbook 3" +\
            "\n Bon appetit :hotdog:"
    elif command.startswith(ADDED):
        response = add_hotdogs(hotdog_req, user_tag)
    elif command.startswith(BOOK):
        response = handle_book(hotdog_req, user_tag)
    elif command.startswith(UNBOOK):
        response = cancel_book(hotdog_req, user_tag)

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=channel,
        text=response
    )


def notify(user_tag):
    global CHANNEL_GLOB

    response = "Hot dog ready for " + user_tag

    slack_client.api_call(
        "chat.postMessage",
        channel=CHANNEL_GLOB,
        text=response
    )


def thief():
    intro_msg = json.dumps(
        [{"image_url": "https://image.ibb.co/nFuqzp/thief.jpg",
          "title": "Hot Dog thief"}])

    # Sends the response back to the channel
    slack_client.api_call(
        "chat.postMessage",
        channel=CHANNEL_GLOB,
        attachments=intro_msg
    )


def get_number(command):
    match = re.search('(?:added|book|unbook) (\d+)', command)
    if match:
        return int(match.group(1))
    return 1


def handle_book(hotdog_req, user_tag):
    global HOTDOGS_BOOKED
    global BOOKED_DICTIONARY
    if HOTDOGS_BOOKED < HOTDOGS_INSIDE:
        HOTDOGS_BOOKED += hotdog_req

        BOOKED_DICTIONARY[user_tag] = datetime.datetime.now() + datetime.timedelta(seconds=10)
        EXTRA_DICT[user_tag] = datetime.datetime.now() + datetime.timedelta(seconds=10)
        return "Booked " + str(hotdog_req) + " hot dog(s) for " + user_tag + "."
    else:
        return "Not enough hot dogs in the machine. Please go add some and let me know!"


def add_hotdogs(hotdogs_added, user_tag):
    global HOTDOGS_INSIDE
    global HOTDOG_MAX
    if hotdogs_added <= (HOTDOG_MAX - HOTDOGS_INSIDE):
        HOTDOGS_INSIDE += hotdogs_added
        return user_tag + ", " + str(hotdogs_added) + " hot dogs added"
    else:
        return "Hey " + user_tag + " hot dog capacity is full!"


def cancel_book(hotdog_req, user_tag):
    global HOTDOGS_BOOKED
    HOTDOGS_BOOKED -= hotdog_req

    del BOOKED_DICTIONARY[user_tag]
    del EXTRA_DICT[user_tag]

    if HOTDOGS_BOOKED < 0:
        HOTDOGS_BOOKED = 0
    return "Hey" + user_tag + ", " + str(hotdog_req) + " hot dogs unbooked and are now free for the take!"


if __name__ == "__main__":
    if slack_client.rtm_connect():
        print("Starter Bot connected and running!")
        # Read bot's user ID by calling Web API method `auth.test`
        starterbot_id = slack_client.api_call("auth.test")["user_id"]

        while True:
            listOfKeys = []
            for key in BOOKED_DICTIONARY:
                if BOOKED_DICTIONARY.get(key) <= datetime.datetime.now():
                    notify(key)
                    listOfKeys.append(key)
            if listOfKeys:
                for i in range(len(listOfKeys)):
                    if key in BOOKED_DICTIONARY:
                        del BOOKED_DICTIONARY[listOfKeys[i]]

            secondList = []
            for key in EXTRA_DICT:
                if EXTRA_DICT.get(key) + datetime.timedelta(seconds=8) <= datetime.datetime.now():
                    thief()
                    secondList.append(key)
            if secondList:
                for i in range(len(secondList)):
                    if key in EXTRA_DICT:
                        del EXTRA_DICT[secondList[i]]

            command, channel, user = parse_bot_commands(slack_client.rtm_read())
            if command:
                handle_command(command, channel, user)
            time.sleep(RTM_READ_DELAY)
    else:
        print("Connection failed. Exception traceback printed above.")



